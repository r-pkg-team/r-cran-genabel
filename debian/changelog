r-cran-genabel (1.8-0-7) unstable; urgency=medium

  * Packaging update
  * Standards-Version: 4.6.2 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Fix build
    Closes: #1042150
  * Lintian-overrides (see lintian bug #1017966)

 -- Andreas Tille <tille@debian.org>  Fri, 18 Aug 2023 08:22:29 +0200

r-cran-genabel (1.8-0-6) unstable; urgency=medium

  * Disable reprotest
  * Replace PROBLEM by REprintf
    Closes: #1008356

 -- Andreas Tille <tille@debian.org>  Mon, 04 Apr 2022 09:14:18 +0200

r-cran-genabel (1.8-0-5) unstable; urgency=medium

  * Package 'GenABEL’ was removed from the CRAN repository.
    The code was obtained from the archive.
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 29 Jan 2022 21:22:05 +0100

r-cran-genabel (1.8-0-4) unstable; urgency=medium

  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Thu, 03 Sep 2020 16:13:03 +0200

r-cran-genabel (1.8-0-3) unstable; urgency=medium

  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Fri, 15 Jun 2018 10:54:59 +0200

r-cran-genabel (1.8-0-2) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * debhelper 10
  * Convert from cdbs to dh-r
  * Secure URI in watch file
  * Testsuite: autopkgtest-pkg-r
  * Canonical homepage for CRAN packages

 -- Andreas Tille <tille@debian.org>  Thu, 19 Oct 2017 14:35:38 +0200

r-cran-genabel (1.8-0-1) unstable; urgency=low

  [ Charles Plessy ]
  * debian/control: removed myself from Uploaders.

  [ Andreas Tille ]
  * New upstream version
  * Moved debian/upstream to debian/upstream/metadata
  * cme fix dpkg-control
  * More detailed copyright

 -- Andreas Tille <tille@debian.org>  Thu, 07 Aug 2014 17:30:04 +0200

r-cran-genabel (1.7-6-1) unstable; urgency=low

  * New upstream version
  * debian/control: Drop citation from long description because this
    information is provided in debian/upstream.

 -- Andreas Tille <tille@debian.org>  Mon, 22 Jul 2013 09:22:48 +0200

r-cran-genabel (1.7-4-1) unstable; urgency=low

  * New upstream version
  * Updated qvalue package name (moved from CRAN to Bioconductor).
  * Drop references to old name r-other-genabel.
  * Drop build-dependance on package only suggested.
  * Normalised VCS URLs following Lintian's standard.
  * Acquire R:Depends directly from r-base-dev.
  * Normalised control file with cme.

 -- Charles Plessy <plessy@debian.org>  Wed, 01 May 2013 14:58:43 +0900

r-cran-genabel (1.7-3-1) unstable; urgency=low

  * New upstream version
  * Replace debian/README.Debian by debian/README.test
  * DEP5 copyright
  * Standards-Version: 3.9.4 (no changes needed)
  * debhelper 9 (control+compat)

 -- Andreas Tille <tille@debian.org>  Sat, 02 Feb 2013 12:32:39 +0100

r-cran-genabel (1.7-0-3) testing-proposed-updates; urgency=low

  * Upload to testing-proposed-updates

 -- Andreas Tille <tille@debian.org>  Sat, 02 Feb 2013 07:08:19 +0000

r-cran-genabel (1.7-0-2) unstable; urgency=low

  * Fixing the problem which prevents the package from loading while
    checking the version on CRAN
    Closes: #699260

 -- Andreas Tille <tille@debian.org>  Wed, 30 Jan 2013 13:11:06 +0100

r-cran-genabel (1.7-0-1) unstable; urgency=low

  * New upstream release.
  * Depends and Suggests packages according to DESCRIPTION (debian/control).
  * Corrected VCS URLs (debian/control).
  * Conforms with Policy 3.9.2 (debian/control, no other changes needed).
  * Build-Depend on r-cran-mass instead of the whole r-recommended.

 -- Charles Plessy <plessy@debian.org>  Fri, 13 Jan 2012 13:56:32 +0900

r-cran-genabel (1.6-5-1) unstable; urgency=low

  * New upstream release, buildable with R > 2.12 (closes: #620269).

  [ Andreas Tille ]
  * debian/control:
    - added myself to uploaders
    - changed section to gnu-r
    - Depend on ${R:Depends}
  * debian/rules:
    - fix permissions of files that should not be executable
    - Require a number equal or superior than the R version the package
      was built with, using a R:Depends substvar

  [ Charles Plessy ]
  * debian/control:
    - Added myself to uploaders.
    - Use debhelper 8 (debian/compat).
    - Build-depend on R > 2.10 according to DESCRIPTION.
    - Replaces r-other-genabel.
    - Added Vcs-Browser and Vcs-Svn fields.
    - Updated homepage.
  * Install CHANGES.LOG as upstream changelog (debian/rules).
  * Converted debian/copyright to DEP-5 format.

 -- Charles Plessy <plessy@debian.org>  Fri, 01 Apr 2011 13:46:37 +0900

r-cran-genabel (1.6-4-1) unstable; urgency=low

  * New upstream release.
  * The package has now arrived in CRAN and is taken from there,
    which according to the R policy is also reflected by the name change.
  * Switch to source/format 3.0 (quilt)
  * Bumped policy-standard to 3.9.1.

 -- Steffen Moeller <moeller@debian.org>  Sat, 05 Feb 2011 18:06:24 +0100

r-other-genabel (1.4-0-1) unstable; urgency=low

  * Initial release (Closes: #492044).

 -- Steffen Moeller <moeller@debian.org>  Wed, 23 Jul 2008 15:43:37 +0200
